﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise6
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new List<string> { "red", "blue", "orange", "white", "black" };
            colours.Add("yellow");
            colours.Add("green");
            colours.Add("purple");

            Console.WriteLine(string.Join(", ", colours));

        }
    }
}
